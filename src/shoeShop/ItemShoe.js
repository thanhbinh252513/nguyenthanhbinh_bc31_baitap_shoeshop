import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, description } = this.props.shoeData;
    return (
      <div className="col-3 ">
        <div>
          <div className="card" style={{ width: "18rem" }}>
            <img className="card-img-top" src={image} alt="Card image cap" />
            <div className="card-body">
              <h5 className="card-title">{name}</h5>
              <p className="card-text">
                {description.length < 30
                  ? description
                  : description.slice(0, 30) + "..."}
              </p>
              <button
                onClick={() => {
                  this.props.handleAddToCard(this.props.shoeData);
                }}
                className="btn btn-primary"
              >
                Add to card
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
