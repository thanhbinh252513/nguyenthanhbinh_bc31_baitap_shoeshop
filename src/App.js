import logo from "./logo.svg";
import "./App.css";
import Ex_shoeShop from "./shoeShop/Ex_shoeShop";
import ItemShoe from "./shoeShop/ItemShoe";
import TableGioHang from "./shoeShop/TableGioHang";

function App() {
  return (
    <div className="App">
      <Ex_shoeShop />
    </div>
  );
}

export default App;
